package excercise4;

import java.lang.reflect.Proxy;

import Core.IExcercise;

public class Excercise41 implements IExcercise
{
	@Override
	public void StartExcercise() 
	{
		System.out.println("Start excercise41\n");
		
        Test1 test1 = new Test1();
        Test2 test2 = new Test2();

        Testable test1Proxy = (Testable) Proxy.newProxyInstance(
                test1
                	.getClass()
                	.getClassLoader(),
                new Class[]{Testable.class},
                new DynamicProxy(test1));

        Testable test2Proxy = (Testable) Proxy.newProxyInstance(
                test1
	                .getClass()
	                .getClassLoader(),
                new Class[]{Testable.class},
                new DynamicProxy(test2));

//        Testable test1Proxy = new Test1();
//
//        Testable test2Proxy = new Test2();
        
        test1Proxy.test();
        test2Proxy.test();
		
	}

}
