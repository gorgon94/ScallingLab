package excercise3;

import Core.IExcercise;

public class Excercise31 implements IExcercise
{
	@SuppressWarnings("unchecked")
	@Override
	public void StartExcercise() 
	{
		System.out.println("Start excercise31\n");
		
        Cache testCache = new Cache();
        for (int i = 1; i <= 10; i++) 
        {
            testCache.put("key " + i, "value " + i);
            System.out.println(testCache);
        }

        for (int i = 1; i <= 10; i++) 
        {
            testCache.put("key " + i, 10);
            System.out.println(testCache);
        }		
	}

}
