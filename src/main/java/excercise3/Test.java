package excercise3;

public class Test implements Testable {

    public String publicStringField = "public string";
    
	@SuppressWarnings("unused")
	private String privateStringField = "private string";

    @Override
    public void test() {
        System.out.println("!!! TEST !!!");
    }
}