package excercise3;

import java.lang.reflect.Field;
import java.lang.reflect.Method;

import Core.IExcercise;

public class Excercise32 implements IExcercise
{
	@Override
	public void StartExcercise() throws Exception
	{
		System.out.println("Start excercise32\n");
		
        Object testObject = new Test();

        Method method = testObject.getClass().getMethod("test");
        method.invoke(testObject);

        Field publicStringField = testObject.getClass().getDeclaredField("publicStringField");
        System.out.println("public field value = " + publicStringField.get(testObject));

        Field privateStringField = testObject.getClass().getDeclaredField("privateStringField");
        privateStringField.setAccessible(true);
        System.out.println("private field value = " + privateStringField.get(testObject));
	}

}
