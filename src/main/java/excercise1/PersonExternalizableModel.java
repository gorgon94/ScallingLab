package excercise1;

import java.io.Externalizable;
import java.io.IOException;
import java.io.ObjectInput;
import java.io.ObjectOutput;

public class PersonExternalizableModel implements Externalizable
{
	private String firstName;
	private String lastName;
	private String birthday;
	private int age;
	
	public String getFirstName() {
		return firstName;
	}
	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}
	public String getLastName() {
		return lastName;
	}
	public void setLastName(String lastName) {
		this.lastName = lastName;
	}
	public String getBirthday() {
		return birthday;
	}
	public void setBirthday(String birthday) {
		this.birthday = birthday;
	}
	public int getAge() {
		return age;
	}
	public void setAge(int age) {
		this.age = age;
	}
		
	@Override
	public String toString() { 
	    return "firstName: " + this.firstName + ", lastName: " + this.lastName + ", birthday: " + this.birthday + ", age: "+this.age;
	}
	
	public void readExternal(ObjectInput objectInput) throws IOException, ClassNotFoundException {
		firstName = (String) objectInput.readObject();
		lastName = (String) objectInput.readObject();
		birthday = (String) objectInput.readObject();
		age = objectInput.readInt();		
	}
	
	public void writeExternal(ObjectOutput objectOutput) throws IOException {
		objectOutput.writeObject(firstName);
		objectOutput.writeObject(lastName);
		objectOutput.writeObject(birthday);
		objectOutput.writeInt(age);		
	}
	
}
