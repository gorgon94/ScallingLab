package excercise1;

import java.io.Serializable;

public class PersonSerializableModel implements Serializable
{
	private static final long serialVersionUID = 1L;
	
	private String firstName;
	private String lastName;
	private String birthday;
	private int age;
	
	public String getFirstName() {
		return firstName;
	}
	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}
	public String getLastName() {
		return lastName;
	}
	public void setLastName(String lastName) {
		this.lastName = lastName;
	}
	public String getBirthday() {
		return birthday;
	}
	public void setBirthday(String birthday) {
		this.birthday = birthday;
	}
	public int getAge() {
		return age;
	}
	public void setAge(int age) {
		this.age = age;
	}
	
	@Override
	public String toString() { 
	    return "firstName: " + this.firstName + ", lastName: " + this.lastName + ", birthday: " + this.birthday + ", age: "+this.age;
	} 
}
