package excercise1;

import java.io.BufferedReader;
import java.io.DataInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.RandomAccessFile;
import java.nio.MappedByteBuffer;
import java.nio.channels.FileChannel;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.Random;

import Core.IExcercise;

public class Excercise12 implements IExcercise
{
	@Override
	public void StartExcercise()
	{
		System.out.println("Start excercise12\n");
		
		String filePath = "random.txt";
		double fileSizeInMb = 100;
		long startTime;
		try 
		{
			startTime = System.currentTimeMillis();
			System.out.println("\nGenerating sample byte data");		
			GenerateData(filePath, fileSizeInMb);
			System.out.println("Data generated: " + (System.currentTimeMillis() - startTime));
			
			startTime = System.currentTimeMillis();
			System.out.println("\nReading Buffered");
			ReadBuffered(filePath);
			System.out.println("Buffered reading time: " + (System.currentTimeMillis() - startTime));
			
			startTime = System.currentTimeMillis();
			System.out.println("\nReading NIO");
			ReadNio(filePath);
			System.out.println("Nio reading time: " + (System.currentTimeMillis() - startTime));
			
			startTime = System.currentTimeMillis();
			System.out.println("\nReading Mapped");
			ReadMemmoryMapped(filePath);
			System.out.println("Mapped reading time: " + (System.currentTimeMillis() - startTime));
		} 
		catch (IOException e) 
		{
			e.printStackTrace();
		}
	}
	
	private void GenerateData(String filePath, double sizeInMb) throws IOException
	{
		Random random = new Random();
		int sizeInBytes = (int)sizeInMb*1024*1024;
		byte[] data = new byte[sizeInBytes];	
		random.nextBytes(data);			
		FileOutputStream fos = new FileOutputStream(new File(filePath));
		fos.write(data, 0, data.length);
		fos.flush();
		fos.close();	
	}
	
	private void ReadBuffered(String filePath) throws IOException
	{
		  FileInputStream fstream = new FileInputStream(filePath);
		  DataInputStream in = new DataInputStream(fstream);
		  BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(in));		  
		  StringBuilder stringBuilder = new StringBuilder();  
		  String line;
		  while( (line = bufferedReader.readLine()) != null ) 
		  {	  
			  stringBuilder.append(new String(line));
		  }		  
		  fstream.close();
		  bufferedReader.close();
		  in.close();	  
	}
	
	private void ReadNio(String filePath) throws IOException
	{
		byte[] data = Files.readAllBytes(Paths.get(filePath));
		System.out.println(data);
	}
	
	private void ReadMemmoryMapped(String filePath) throws IOException
	{
        File file = new File(filePath);
        RandomAccessFile randomAccessFile =  new RandomAccessFile(file, "r");
        FileChannel fileChannel = randomAccessFile.getChannel();      
        MappedByteBuffer buffer = fileChannel.map(FileChannel.MapMode.READ_ONLY, 0, fileChannel.size());
        System.out.println(buffer);
        randomAccessFile.close();
	}}
