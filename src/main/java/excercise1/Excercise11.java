package excercise1;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;

import Core.IExcercise;

public class Excercise11 implements IExcercise
{
	@Override
	public void StartExcercise()
	{
		System.out.println("Start excercise11\n");
		
		try 
        {
			PersonSerializableModel serializablePerson = new PersonSerializableModel();
			serializablePerson.setFirstName("Miłosz");
			serializablePerson.setLastName("Zawadowski");
			serializablePerson.setAge(24);
			serializablePerson.setBirthday("1994-01-20");
			
			PersonExternalizableModel externalizablePerson = new PersonExternalizableModel();
			externalizablePerson.setFirstName("Włodek");
			externalizablePerson.setLastName("Kowalski");
			externalizablePerson.setAge(22);
			externalizablePerson.setBirthday("1996-01-20");
			
			String serializableFilePath = "serializable-person.txt";
			String externalizableFilePath = "externalizable-person.txt";
			
			Serialize(serializablePerson, serializableFilePath);
			Serialize(externalizablePerson, externalizableFilePath);
			
			PersonSerializableModel newSerializablePerson = (PersonSerializableModel) Deserialize(serializableFilePath);
			PersonExternalizableModel newExternalizablePerson = (PersonExternalizableModel) Deserialize(externalizableFilePath);
			
			System.out.println(serializablePerson);
			System.out.println(externalizablePerson);
			System.out.println();
			System.out.println(newSerializablePerson);
			System.out.println(newExternalizablePerson);
		} 
        catch (Exception e) 
        {
			e.printStackTrace();
		}
    }
	    
    private static void Serialize(Object obj, String filePath) throws IOException
    {
		FileOutputStream fout = new FileOutputStream(filePath);
		ObjectOutputStream oos = new ObjectOutputStream(fout);			  
		oos.writeObject(obj);
		oos.close();
    }

    private static Object Deserialize(String filePath) throws IOException, ClassNotFoundException
    {
	    FileInputStream fi = new FileInputStream(filePath);
	    ObjectInputStream si = new ObjectInputStream(fi);  	    
		Object obj =  si.readObject();  	
		si.close();
		return obj;
    }
}
