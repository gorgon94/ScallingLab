package Core;

import com.google.common.base.Stopwatch;

public class Benchmark {
	
	public void StartBenchmark(Runnable runnableMethod)
	{
		Stopwatch stopwatch = Stopwatch.createStarted();
		runnableMethod.run();
		stopwatch.stop();
		System.out.println("Benchmark time "+stopwatch.toString());
	}
}
