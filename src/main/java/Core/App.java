package Core;

import excercise1.Excercise11;
import excercise1.Excercise12;
import excercise2.Excercise21;
import excercise2.Excercise22;
import excercise3.Excercise31;
import excercise3.Excercise32;
import excercise4.Excercise41;
import excercise4.Excercise42;

public class App 
{
    public static void main( String[] args )
    {
    	try
    	{
	    	int whichExcercise = 32;
	    	
	    	switch(whichExcercise)
	    	{
	    		case 11: new Excercise11().StartExcercise(); break;
	    		case 12: new Excercise12().StartExcercise(); break;
	    		
	    		case 21: new Excercise21().StartExcercise(); break;
	    		case 22: new Excercise22().StartExcercise(); break;
	    		
	    		case 31: new Excercise31().StartExcercise(); break;
	    		case 32: new Excercise32().StartExcercise(); break;
	    		
	    		case 41: new Excercise41().StartExcercise(); break;
	    		case 42: new Excercise42().StartExcercise(); break;
	    	}
	    }
    	catch(Exception e)
    	{
    		e.printStackTrace();
    	}
    }
}
