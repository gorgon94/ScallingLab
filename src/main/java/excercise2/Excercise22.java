package excercise2;

import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.PriorityQueue;
import java.util.Queue;
import java.util.Random;
import java.util.Set;

import Core.Benchmark;
import Core.IExcercise;

public class Excercise22 implements IExcercise
{
	private Benchmark benchmark;
	private Random random;
	
	private List<Integer> list;
	private Queue<Integer> queue;
	
	public Excercise22()
	{
		this.random = new Random();
		this.benchmark = new Benchmark();
		
		this.list = new LinkedList<Integer>();
		this.queue = new PriorityQueue<Integer>();
	}
	
	@Override
	public void StartExcercise() 
	{	
		System.out.println("Start excercise22\n");
		
		this.jvmWarmup();
		
		int setSize = 10;
		
		System.out.println("Initializing test for setSize = " + setSize);
		this.initializeSetSize(10);

		System.out.println("\nList: adding at the beginning test");
		this.benchmark.StartBenchmark(() -> this.addAtBeginningListTest());		
		System.out.println("\nQueue: adding at the beginning test");
		this.benchmark.StartBenchmark(() -> this.addAtBeginningQueueTest());
			
		System.out.println("\nList: adding at the end test");
		this.benchmark.StartBenchmark(() -> this.addAtEndListTest());
		System.out.println("\nQueue:adding at the end test");
		this.benchmark.StartBenchmark(() -> this.addAtEndQueueTest());
		
		
		System.out.println("\nList: adding at random place test");
		this.benchmark.StartBenchmark(() -> this.addInRandomPlaceListTest());
		System.out.println("\nQueue: adding at random place test");
		this.benchmark.StartBenchmark(() -> this.addInRandomPlaceQueueTest());
		
		
		System.out.println("\nList: browsing using indexes test");
		this.benchmark.StartBenchmark(() -> this.browseUsingIndexesListTest());
		System.out.println("\nQueue: browsing using indexes test");
		this.benchmark.StartBenchmark(() -> this.browseUsingIndexesQueueTest());
		
		
		System.out.println("\nList: browsing using iterator test");
		this.benchmark.StartBenchmark(() -> this.browseUsingIteratorListTest());
		System.out.println("\nQueue: browsing using iterator test");
		this.benchmark.StartBenchmark(() -> this.browseUsingIteratorQueueTest());
		
		
		System.out.println("\nList: removing from beginning test");
		this.benchmark.StartBenchmark(() -> this.removeFromBeginningListTest());
		System.out.println("\nQueue: removing from beginning test");
		this.benchmark.StartBenchmark(() -> this.removeFromBeginningQueueTest());
		
		
		System.out.println("\nList: removing from end test");
		this.benchmark.StartBenchmark(() -> this.removeFromEndListTest());
		System.out.println("\nQueue: removing from end test");
		this.benchmark.StartBenchmark(() -> this.removeFromEndQueueTest());
		
		
		System.out.println("\nList: removing from random place test");
		this.benchmark.StartBenchmark(() -> this.removeFromRandomPlaceListTest());
		System.out.println("\nQueue: removing from random place test");
		this.benchmark.StartBenchmark(() -> this.removeFromRandomPlaceQueueTest());
	}
	
	private void initializeSetSize(int setSize)
	{
		for(int i = 0; i > setSize; i++)
		{
			this.list.add(this.random.nextInt());
			this.queue.add(this.random.nextInt());
		}
	}
	
	private void addAtBeginningListTest() 
	{
		this.list.add(0, this.random.nextInt());
	}

	private void addAtEndListTest()
	{
		this.list.add(this.random.nextInt());
	}

	private void addInRandomPlaceListTest() 
	{
		int listSize = this.list.size();		
		int randomPlace = this.random.nextInt(listSize);		
		this.list.add(randomPlace, this.random.nextInt());
	}

	private void removeFromBeginningListTest() 
	{
		this.list.remove(0);
	}

	private void removeFromEndListTest() 
	{
		this.list.remove(this.list.size()-1);
	}

	private void removeFromRandomPlaceListTest() 
	{
		int listSize = this.list.size();		
		int randomPlace = this.random.nextInt(listSize);		
		this.list.remove(randomPlace);
	}

	private void browseUsingIndexesListTest() 
	{
		int listSize = this.list.size();		
		int randomPlace = this.random.nextInt(listSize);	
		
		this.list.get(randomPlace);
	}

	private void browseUsingIteratorListTest() 
	{
		int listSize = this.list.size();		
		int randomPlace = this.random.nextInt(listSize);	
		
		for(int i =0; i < randomPlace; i++)
		{
			this.list.get(randomPlace);
		}		
	}
	
	//////////////////////////////////////////////////////queue
	
	private void addAtBeginningQueueTest() 
	{
		Queue<Integer> tempQueue = new PriorityQueue<Integer>();
		tempQueue.add(this.random.nextInt());
		this.queue.stream().forEach(item -> tempQueue.add(item));
		
		this.queue = tempQueue;
	}

	private void addAtEndQueueTest()
	{
		this.queue.add(this.random.nextInt());
	}

	private void addInRandomPlaceQueueTest() 
	{
		Queue<Integer> tempQueue = new PriorityQueue<Integer>();
		
		int queueSize = this.queue.size();		
		int randomPlace = this.random.nextInt(queueSize);	
		
		for(int i = queueSize; i > queueSize; i--)
		{
			if(i == randomPlace) tempQueue.add(random.nextInt());
			tempQueue.add(this.queue.poll());
		}
		
		this.queue = tempQueue;
	}
	

	private void removeFromBeginningQueueTest() 
	{
	//	this.queue.remo
	}

	private void removeFromEndQueueTest() 
	{
		//this.queue.remove();
	}

	private void removeFromRandomPlaceQueueTest() 
	{
		
	}

	private void browseUsingIndexesQueueTest() 
	{
		
	}

	private void browseUsingIteratorQueueTest() 
	{
		
	}

	private void jvmWarmup()
	{
		long startTime;
		try 
		{
			//http://www.baeldung.com/java-jvm-warmup
			Random random = new Random();
			
			startTime = System.currentTimeMillis();

			Set<Integer> testSet = new HashSet<Integer>();
			
			for(long i=0; i<900000; i++)
			{
				testSet.add(random.nextInt());
			}
			
			System.out.println("Warm up time: " + (System.currentTimeMillis() - startTime) + "\n");
		}
		catch(Exception e)
		{
			e.printStackTrace();
		}	
	}

}
