package excercise2;

import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedHashSet;
import java.util.Random;
import java.util.Set;
import java.util.TreeSet;

import Core.Benchmark;
import Core.IExcercise;

public class Excercise21 implements IExcercise
{
	private Benchmark benchmark;
	
    private HashSet<Integer> hashSet;
    private LinkedHashSet<Integer> linkedHashSet;
    private TreeSet<Integer> treeSet;
    
	@Override
	public void StartExcercise() 
	{
		System.out.println("Start excercise21\n");
		
		this.jvmWarmup();
		this.initialize();
		this.startTest();
	}
	
	private void initialize() 
	{
		this.benchmark = new Benchmark();
		this.hashSet = new HashSet<>();
		this.linkedHashSet = new LinkedHashSet<>();
		this.treeSet = new TreeSet<>();	
	}

	private void startTest() {
		this.addingTest();
		this.browsingTest();
		this.checkingIfExistsTest();
		this.removingTest();		
	}

	
	private void addingTest()
	{
		System.out.println("\nhashSet addingTest");
		this.benchmark.StartBenchmark(() -> this.add(this.hashSet));
		
		System.out.println("\nlinkedHashSet addingTest");
		this.benchmark.StartBenchmark(() -> this.add(this.linkedHashSet));
		
		System.out.println("\ntreeSet addingTest");
		this.benchmark.StartBenchmark(() -> this.add(this.treeSet));
	}

	private void browsingTest()
	{
		System.out.println("\nhashSet browsingTest");
		this.benchmark.StartBenchmark(() -> this.browse(this.hashSet));
		
		System.out.println("\nlinkedHashSet browsingTest");
		this.benchmark.StartBenchmark(() -> this.browse(this.linkedHashSet));
		
		System.out.println("\ntreeSet browsingTest");
		this.benchmark.StartBenchmark(() -> this.browse(this.treeSet));
	}
	
	private void checkingIfExistsTest()
	{
		System.out.println("\nhashSet checkingIfExistsTest");
		this.benchmark.StartBenchmark(() -> this.checkIfExists(this.hashSet));
		
		System.out.println("\nlinkedHashSet checkingIfExistsTest");
		this.benchmark.StartBenchmark(() -> this.checkIfExists(this.linkedHashSet));
		
		System.out.println("\ntreeSet checkingIfExistsTest");
		this.benchmark.StartBenchmark(() -> this.checkIfExists(this.treeSet));
	}
	
	private void removingTest()
	{
		System.out.println("\nhashSet removingTest");
		this.benchmark.StartBenchmark(() -> this.remove(this.hashSet));
		
		System.out.println("\nlinkedHashSet removingTest");
		this.benchmark.StartBenchmark(() -> this.remove(this.linkedHashSet));
		
		System.out.println("\ntreeSet removingTest");
		this.benchmark.StartBenchmark(() -> this.remove(this.treeSet));
	}
	
    private void add(Set<Integer> set) {
        set.add(999999999);
    }

    private void remove(Set<Integer> set) {
        Iterator<Integer> iterator = set.iterator();
        Object middleElement = null;
        for (int i = 0; i < set.size(); i++) {
            middleElement = iterator.next();
        }
        set.remove(middleElement);
    }

    private void browse(Set<Integer> set) {
        Iterator<Integer> iterator = set.iterator();
        while (iterator.hasNext()) {
            iterator.next();
        }
    }

    private boolean checkIfExists(Set<Integer> set) {
        return set.contains(999999998);
    }
	
	private void jvmWarmup()
	{
		long startTime;
		try 
		{
			//http://www.baeldung.com/java-jvm-warmup
			Random random = new Random();
			
			startTime = System.currentTimeMillis();

			Set<Integer> testSet = new HashSet<Integer>();
			
			for(long i=0; i<900000; i++)
			{
				testSet.add(random.nextInt());
			}
			
			System.out.println("Warm up time: " + (System.currentTimeMillis() - startTime) + "\n");
		}
		catch(Exception e)
		{
			e.printStackTrace();
		}	
	}
}
